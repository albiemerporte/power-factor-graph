import random
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication, QVBoxLayout, QWidget
from PyQt6.QtCore import QTimer
import pyqtgraph as pg
import math

class MyChart:
    def __init__(self):
        self.mainui = loadUi('widchart.ui')
        self.mainui.show()
        self.mainui.pbHide.clicked.connect(self.hideboxchart)

        self.plot_widget = pg.PlotWidget()
        self.mainui.qvboxChart.addWidget(self.plot_widget)

        self.plot_chart()

        self.timer = QTimer()
        self.timer.timeout.connect(self.update_chart)
        self.timer.start(1000)  # 10 seconds in milliseconds

    def hideboxchart(self):
        self.mainui.qvboxChart.parentWidget().hide()

    def plot_chart(self):
        self.x = [random.uniform(0, 1) for _ in range(10)]  # Random floats between 0 and 1
        self.y = [math.cos(x * math.pi) for x in self.x]

        self.plot_widget.plot(self.x, self.y, pen='b', symbol='o', symbolSize=10, symbolBrush='r')

    def update_chart(self):
        self.x = [random.uniform(0, 1) for _ in range(10)]  # Random floats between 0 and 1
        self.y = [math.cos(x * math.pi) for x in self.x]  # Random integers between 1 and 100

        # Clear the existing plot and plot the new data
        self.plot_widget.clear()
        self.plot_widget.plot(self.x, self.y, pen='b', symbol='o', symbolSize=10, symbolBrush='r')

        self.power_factor = self.compute_power_factor(self.x, self.y)

        # Print the generated x and y values and the power factor
        #print("Generated x values (as percentages):", self.x)
        #print("Generated y values (as percentages):", self.y)
        self.mainui.lblPfResult.setText("Power Factor:     {:.2f}".format(self.power_factor))

    def compute_power_factor(self, x, y):
        # Compute the phase angle (in radians)
        phase_angle = math.atan2(sum(y), sum(x))
        # Compute the power factor (cosine of the phase angle)
        power_factor = math.cos(phase_angle)
        return power_factor

if __name__ == "__main__":
    app = QApplication([])
    main = MyChart()
    app.exec()
